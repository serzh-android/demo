package com.erminesoft.my_account.myacount.core;

import android.content.Context;
import android.content.SharedPreferences;

public final class SharedHelper {

    private static final String NAME = "my_account";

    private static final String IS_FIRST_START = "is_first_start";
    private static final String USER_LOGIN = "user_login";
    private static final String USER_PASSWORD = "user_password";
    private static final String CURRENCY_SYMBOL = "currency_symbol";

    private final SharedPreferences preferences;

    public SharedHelper(Context context) {
        preferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    public boolean isFirstStart() {
        boolean isFirst =  preferences.getBoolean(IS_FIRST_START, true);
        preferences.edit().putBoolean(IS_FIRST_START, false).apply();
        return isFirst;
    }

    public void setCurrencySymbol(int symbolId) {
        preferences.edit().putInt(CURRENCY_SYMBOL, symbolId).apply();
    }

    public int getCurrencySymbol() {
        return preferences.getInt(CURRENCY_SYMBOL, 0);
    }


    public void sharedHelperClear() {
        preferences.edit().clear().apply();
    }

}
