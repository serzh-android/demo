package com.erminesoft.my_account.myacount.core.bridge;

import android.database.Cursor;

import com.erminesoft.my_account.myacount.core.AAplication;
import com.erminesoft.my_account.myacount.model.HistoryDay;
import com.erminesoft.my_account.myacount.model.HistoryMonth;

import java.util.List;

public interface ActivityBridge {

    AAplication getUApplication();

    List<String> getListDates();


    AAplication.HistoryState getHistoryState();

    List<HistoryDay> getDayOfMonthList(String dateMonth);

    List<HistoryMonth> getMonthOfYearList(String dateYear);

    void pagerItemPressed(int position);

    Cursor getHistoryByDate(String date);


}
