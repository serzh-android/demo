package com.erminesoft.my_account.myacount.core.bridge;

import android.database.Cursor;

import java.util.Observer;


public interface DBbridge {
    Cursor loadOutcome();

    Cursor loadIncome();

    Cursor calculateSumExpenses();

    Cursor calculateSumExpensesByDay(String date);

    Cursor calculateSumExpensesByMonth(String date);

    Cursor calculateSumIncome();

    Cursor calculateSumIncomeByDay(String date);

    Cursor calculateSumIncomeByMonth(String date);

    Cursor loadOutcomeCategories();

    boolean isAddedOutcomeCategoriesByName(String name);

    Cursor loadIncomeCategories();

    Cursor loadCategoryById(int iD, String catType);

    boolean isAddedIncomesCategoriesByName(String name);

    void saveOutcomeToDb(int Categoiries, String fieldNameCosts, int date, int sumCost, boolean isSent);

    void saveIncomeToDb(int Categories, String fieldNameIncome, int date, int sumIncome, boolean isSent);

    void saveCategoriesOutComeToDb(String categoryName,int color, boolean isSent);

    void saveCategoriesIncomeToDb(String categoryName,int color, boolean isSent);

    void updateCategoryIncomeToDb(String categoryName, int catId, int color, boolean isSent);

    void updateCategoryExpensesToDb(String categoryName, int catId, int color, boolean isSent);

    void addNewObserver(Observer observer);

    void removeObserver(Observer observer);

    Cursor getAllHistory();

    Cursor getAllHistoryByDay(String date);

    Cursor getIncomeHistoryByDay(String date);

    Cursor getExpensesHistoryByDay(String date);

    Cursor getIncomeHistoryByMonth(String date);

    Cursor getExpensesHistoryByMonth(String date);

    Cursor getIncomeHistoryByYear(String date);

    Cursor getExpensesHistoryByYear(String date);

    Cursor getAllHistoryByMonth(String date);

    Cursor getAllHistoryByYear(String date);

    Cursor getIncomeHistory();

    Cursor getOutcomeHistory();

    void clearAllData();

    Cursor getAllFields();


    int getFirstDate();

    void deleteOutcomeCategory(int deletedItemId);

    void deleteIncomeCategory(int deletedItemId);

    Cursor getIncomeAvailableColors();

    Cursor getExpensesAvailableColors();
}
