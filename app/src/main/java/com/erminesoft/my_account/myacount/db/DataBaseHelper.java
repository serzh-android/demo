package com.erminesoft.my_account.myacount.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public final class DataBaseHelper extends SQLiteOpenHelper {

    final String LOG_TAG = "myLog";

    public static final String DATABASE_NAME = "Account";
    public static final int DATABASE_VERSION = 2;

    //table income
    public static final String TABLE_INCOME = "income";
    public static final String INCOME_ID = "_id";
    public static final String INCOME_DATE = "income_date";
    public static final String INCOME_NAME = "income_name";
    public static final String INCOME_CATEGORIES = "income_categories";
    public static final String INCOME_SUM = "income_sum";
    public static final String INCOME_IS_SYNC = "is_sync";


    //table outcome
    public static final String TABLE_OUTCOME = "outcome";
    public static final String OUTCOME_ID = "_id";
    public static final String OUTCOME_DATE = "outcome_date";
    public static final String OUTCOME_NAME = "outcome_name";
    public static final String OUTCOME_CATEGORIES = "outcome_categories";
    public static final String OUTCOME_SUM = "outcome_sum";
    public static final String OUTCOME_IS_SYNC = "is_sync";


    //table categories
    public static final String TABLE_CATEGORIES = "categories";
    public static final String CATEGORY_ID = "_id";
    public static final String CATEGORY_TYPE = "categories_type";
    public static final String CATEGORY_NAME = "categories_name";
    public static final String CATEGORY_IMAGE_ID = "categories_image_id";
    public static final String CATEGORY_IS_SYNC = "is_sync";

    // SELECT_ALL_HISTORY
    public static final String ALL_HISTORY_ID = "_id";
    public static final String ALL_HISTORY_NAME = "name";
    public static final String ALL_HISTORY_SUM = "sum";
    public static final String ALL_HISTORY_DATE = "date";
    public static final String ALL_HISTORY_CATEGORY = "category";
    public static final String ALL_HISTORY_CATEGORY_TYPE = "cat_type";
    public static final String ALL_HISTORY_CATEGORY_NAME = "cat_name";
    public static final String ALL_HISTORY_CATEGORY_IMAGE_ID = "cat_image_id";


    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(buildOutComes());
        db.execSQL(buildIncomes());
        db.execSQL(buildCategories());
    }

    private String buildIncomes() {
        return "CREATE TABLE " + TABLE_INCOME + " ( "
                + INCOME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + INCOME_NAME + " text, "
                + INCOME_DATE + " INTEGER, "
                + INCOME_SUM + " INTEGER, "
                + INCOME_IS_SYNC + " BOOLEAN, "
                + INCOME_CATEGORIES + " INTEGER " + " )";
    }

    private String buildOutComes() {
        return "CREATE TABLE " + TABLE_OUTCOME + " ( "
                + OUTCOME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + OUTCOME_NAME + " text, "
                + OUTCOME_DATE + " INTEGER, "
                + OUTCOME_SUM + " INTEGER, "
                + OUTCOME_IS_SYNC + " BOOLEAN, "
                + OUTCOME_CATEGORIES + " INTEGER " + " )";
    }

    private String buildCategories() {
        return "CREATE TABLE " + TABLE_CATEGORIES + " ( "
                + CATEGORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + CATEGORY_NAME + " text, "
                + CATEGORY_IMAGE_ID + " INTEGER, "
                + CATEGORY_IS_SYNC + " BOOLEAN, "
                + CATEGORY_TYPE + " INTEGER " + " )";
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}