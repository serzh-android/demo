package com.erminesoft.my_account.myacount.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.erminesoft.my_account.myacount.core.bridge.DBbridge;
import com.erminesoft.my_account.myacount.util.DateUtil;

import java.text.ParseException;
import java.util.Observable;
import java.util.Observer;

public final class DbManager extends Observable implements DBbridge {

    private static final int OUTCOME_CATEGORIES_TYPE = 0;
    private static final int INCOME_CATEGORIES_TYPE = 1;

    private final String LOG_TAG = "myLog";
    private final DataBaseHelper baseHelper;

    public DbManager(Context context) {
        Log.d(LOG_TAG, "create db manager");
        baseHelper = new DataBaseHelper(context);
    }

    private void notifyObserversProcedure(String tag) {
        setChanged();
        notifyObservers(tag);
    }

    @Override
    public void saveOutcomeToDb(int category, String nameText, int date, int sumCost, boolean isSent) {
        ContentValues cv = Mapper.convertOutcome(nameText, date, category, sumCost, isSent);
        baseHelper.getWritableDatabase().insert(DataBaseHelper.TABLE_OUTCOME, null, cv);
        notifyObserversProcedure("outcome");
    }

    @Override
    public void saveIncomeToDb(int category, String nameText, int date, int sumIncome, boolean isSent) {
        ContentValues cv = Mapper.convertIncome(nameText, date, category, sumIncome, isSent);
        baseHelper.getWritableDatabase().insert(DataBaseHelper.TABLE_INCOME, null, cv);
        notifyObserversProcedure("income");
    }

    @Override
    public void saveCategoriesOutComeToDb(String categoryName, int color, boolean isSent) {
        ContentValues cv = Mapper.convertCategory(categoryName, OUTCOME_CATEGORIES_TYPE, color, isSent);
        baseHelper.getWritableDatabase().insert(DataBaseHelper.TABLE_CATEGORIES, null, cv);
        notifyObserversProcedure("category");
    }

    @Override
    public void saveCategoriesIncomeToDb(String categoryName, int color, boolean isSent) {
        ContentValues cv = Mapper.convertCategory(categoryName, INCOME_CATEGORIES_TYPE, color, isSent);
        baseHelper.getWritableDatabase().insert(DataBaseHelper.TABLE_CATEGORIES, null, cv);
        notifyObserversProcedure("category");
    }

    @Override
    public void updateCategoryIncomeToDb(String categoryName, int catId, int color, boolean isSent) {
        ContentValues cv = Mapper.convertCategory(categoryName, INCOME_CATEGORIES_TYPE, color, isSent);
        baseHelper.getWritableDatabase().update(DataBaseHelper.TABLE_CATEGORIES, cv," _id = ?", new String[]{String.valueOf(catId)} );
        notifyObserversProcedure("category");
    }

    @Override
    public void updateCategoryExpensesToDb(String categoryName, int catId, int color, boolean isSent) {
        ContentValues cv = Mapper.convertCategory(categoryName, OUTCOME_CATEGORIES_TYPE, color, isSent);
        baseHelper.getWritableDatabase().update(DataBaseHelper.TABLE_CATEGORIES, cv," _id = ?", new String[]{String.valueOf(catId)} );
        notifyObserversProcedure("category");
    }


    @Override
    public void addNewObserver(Observer observer) {
        super.addObserver(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        super.deleteObserver(observer);
    }

    @Override
    public void clearAllData() {
        baseHelper.getWritableDatabase().delete(DataBaseHelper.TABLE_OUTCOME, null, null);
        baseHelper.getWritableDatabase().delete(DataBaseHelper.TABLE_INCOME, null, null);
        baseHelper.getWritableDatabase().delete(DataBaseHelper.TABLE_CATEGORIES, null, null);
    }

    @Override
    public Cursor getAllFields() {
        return null;
    }


    @Override
    public Cursor loadOutcome() {
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_COSTS_WITH_CATEGORIES, new String[0]);
    }

    @Override
    public Cursor loadIncome() {
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_INCOMES_WITH_CATEGORIES, new String[0]);
    }

    @Override
    public Cursor loadOutcomeCategories() {
        String[] args = new String[]{String.valueOf(OUTCOME_CATEGORIES_TYPE)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_CATEGORIES_BY_TYPE, args);
    }

    @Override
    public boolean isAddedOutcomeCategoriesByName(String name) {
        String[] args = new String[]{String.valueOf(OUTCOME_CATEGORIES_TYPE), name};
        Cursor cursor = baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_ADDED_COSTS_NAME, args);
        return (cursor.getCount() == 0 ? false : true);
    }

    @Override
    public Cursor loadIncomeCategories() {
        String[] args = new String[]{String.valueOf(INCOME_CATEGORIES_TYPE)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_CATEGORIES_BY_TYPE, args);
    }

    @Override
    public Cursor loadCategoryById(int iD, String catType) {
        String[] args;
        if(catType.equals("income")){
            args = new String[]{String.valueOf(INCOME_CATEGORIES_TYPE), String.valueOf(iD)};
        } else {
            args = new String[]{String.valueOf(OUTCOME_CATEGORIES_TYPE), String.valueOf(iD)};
        }

        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_CATEGORY_BY_TYPE_BY_ID, args);
    }

    @Override
    public boolean isAddedIncomesCategoriesByName(String name) {
        String[] args = new String[]{String.valueOf(INCOME_CATEGORIES_TYPE), name};
        Cursor cursor = baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_ADDED_INCOMES_NAME, args);
        return (cursor.getCount() == 0 ? false : true);
    }

    @Override
    public Cursor calculateSumExpenses() {
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_GENERAL_COSTS_SUM, new String[0]);
    }

    @Override
    public Cursor calculateSumExpensesByDay(String date) {
        int startDateInt = 0;
        int endDateInt = (int) System.currentTimeMillis() / 1000;

        try {
            startDateInt =  DateUtil.getStartOfDay(date);
            endDateInt =  DateUtil.getEndOfDayInMillis(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] whereArgs = new String[]{String.valueOf(startDateInt), String.valueOf(endDateInt)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_GENERAL_COSTS_SUM_BY_DATE, whereArgs);
    }

    @Override
    public Cursor calculateSumExpensesByMonth(String date) {
        int startDateInt = 0;
        int endDateInt = (int) System.currentTimeMillis() / 1000;

        try {
            startDateInt =  DateUtil.getStartOfMonth(date);
            endDateInt =  DateUtil.getEndOfMonthInMillis(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] whereArgs = new String[]{String.valueOf(startDateInt), String.valueOf(endDateInt)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_GENERAL_COSTS_SUM_BY_DATE, whereArgs);
    }



    @Override
    public Cursor calculateSumIncome() {
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_GENERAL_INCOME_SUM, new String[0]);
    }

    @Override
    public Cursor calculateSumIncomeByDay(String date) {
        int startDateInt = 0;
        int endDateInt = (int) System.currentTimeMillis() / 1000;

        try {
            startDateInt =  DateUtil.getStartOfDay(date);
            endDateInt =  DateUtil.getEndOfDayInMillis(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] whereArgs = new String[]{String.valueOf(startDateInt), String.valueOf(endDateInt)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_GENERAL_INCOME_SUM_BY_DATE, whereArgs);
    }

    @Override
    public Cursor calculateSumIncomeByMonth(String date) {
        int startDateInt = 0;
        int endDateInt = (int) System.currentTimeMillis() / 1000;

        try {
            startDateInt =  DateUtil.getStartOfMonth(date);
            endDateInt =  DateUtil.getEndOfMonthInMillis(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        String[] whereArgs = new String[]{String.valueOf(startDateInt), String.valueOf(endDateInt)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_GENERAL_INCOME_SUM_BY_DATE, whereArgs);
    }



    @Override
    public Cursor getAllHistory(){
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_ALL_HISTORY, new String[0]);
    }




    @Override
    public Cursor getAllHistoryByDay(String date)  {
        int startDateInt = 0;
        int endDateInt = (int) System.currentTimeMillis() / 1000;

        try {
            startDateInt =  DateUtil.getStartOfDay(date);
            endDateInt =  DateUtil.getEndOfDayInMillis(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] whereArgs = new String[]{String.valueOf(startDateInt), String.valueOf(endDateInt), String.valueOf(startDateInt), String.valueOf(endDateInt)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_ALL_HISTORY_BY_DATE, whereArgs);
    }

    @Override
    public Cursor getIncomeHistoryByDay(String date)  {
        int startDateInt = 0;
        int endDateInt = (int) System.currentTimeMillis() / 1000;

        try {
            startDateInt =  DateUtil.getStartOfDay(date);
            endDateInt =  DateUtil.getEndOfDayInMillis(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] whereArgs = new String[]{String.valueOf(startDateInt), String.valueOf(endDateInt)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_INCOME_HISTORY_BY_DATE, whereArgs);
    }

    @Override
    public Cursor getExpensesHistoryByDay(String date)  {
        int startDateInt = 0;
        int endDateInt = (int) System.currentTimeMillis() / 1000;

        try {
            startDateInt =  DateUtil.getStartOfDay(date);
            endDateInt =  DateUtil.getEndOfDayInMillis(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] whereArgs = new String[]{String.valueOf(startDateInt), String.valueOf(endDateInt)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_OUTCOME_HISTORY_BY_DATE, whereArgs);
    }



    @Override
    public Cursor getIncomeHistoryByMonth(String date)  {
        int startDateInt = 0;
        int endDateInt = (int) System.currentTimeMillis() / 1000;

        try {
            startDateInt =  DateUtil.getStartOfMonth(date);
            endDateInt =  DateUtil.getEndOfMonthInMillis(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] whereArgs = new String[]{String.valueOf(startDateInt), String.valueOf(endDateInt)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_INCOME_HISTORY_BY_DATE, whereArgs);
    }

    @Override
    public Cursor getExpensesHistoryByMonth(String date)  {
        int startDateInt = 0;
        int endDateInt = (int) System.currentTimeMillis() / 1000;

        try {
            startDateInt =  DateUtil.getStartOfMonth(date);
            endDateInt =  DateUtil.getEndOfMonthInMillis(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] whereArgs = new String[]{String.valueOf(startDateInt), String.valueOf(endDateInt)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_OUTCOME_HISTORY_BY_DATE, whereArgs);
    }



    @Override
    public Cursor getIncomeHistoryByYear(String date)  {
        int startDateInt = 0;
        int endDateInt = (int) System.currentTimeMillis() / 1000;

        try {
            startDateInt =  DateUtil.getStartOfYear(date);
            endDateInt =  DateUtil.getEndOfYearInMillis(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] whereArgs = new String[]{String.valueOf(startDateInt), String.valueOf(endDateInt)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_INCOME_HISTORY_BY_DATE, whereArgs);
    }

    @Override
    public Cursor getExpensesHistoryByYear(String date)  {
        int startDateInt = 0;
        int endDateInt = (int) System.currentTimeMillis() / 1000;

        try {
            startDateInt =  DateUtil.getStartOfYear(date);
            endDateInt =  DateUtil.getEndOfYearInMillis(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] whereArgs = new String[]{String.valueOf(startDateInt), String.valueOf(endDateInt)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_OUTCOME_HISTORY_BY_DATE, whereArgs);
    }



    @Override
    public Cursor getAllHistoryByMonth(String date)  {
        int startDateInt = 0;
        int endDateInt = (int) System.currentTimeMillis() / 1000;

        try {
            startDateInt =  DateUtil.getStartOfMonth(date);
            endDateInt =  DateUtil.getEndOfMonthInMillis(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] whereArgs = new String[]{String.valueOf(startDateInt), String.valueOf(endDateInt), String.valueOf(startDateInt), String.valueOf(endDateInt)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_ALL_HISTORY_BY_DATE, whereArgs);
    }

    @Override
    public Cursor getAllHistoryByYear(String date)  {
        int startDateInt = 0;
        int endDateInt = (int) System.currentTimeMillis() / 1000;

        try {
            startDateInt =  DateUtil.getStartOfYear(date);
            endDateInt =  DateUtil.getEndOfYearInMillis(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] whereArgs = new String[]{String.valueOf(startDateInt), String.valueOf(endDateInt), String.valueOf(startDateInt), String.valueOf(endDateInt)};
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_ALL_HISTORY_BY_DATE, whereArgs);
    }


    @Override
    public Cursor getIncomeHistory() {
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_INCOME_HISTORY, new String[0]);
    }

    @Override
    public Cursor getOutcomeHistory() {
        return baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_OUTCOME_HISTORY, new String[0]);
    }

    @Override
    public void deleteOutcomeCategory(int deletedItemId){
        String whereCat = DataBaseHelper.CATEGORY_ID + " = ?";
        String[] whereArgs = new String[1];
        whereArgs[0] = String.valueOf(deletedItemId);
        baseHelper.getWritableDatabase().delete(DataBaseHelper.TABLE_CATEGORIES, whereCat, whereArgs);

        String whereOutcome = DataBaseHelper.OUTCOME_CATEGORIES + " = ?";
        baseHelper.getWritableDatabase().delete(DataBaseHelper.TABLE_OUTCOME, whereOutcome, whereArgs);

        notifyObserversProcedure("category");
    }

    @Override
    public void deleteIncomeCategory(int deletedItemId){
        String whereCat = DataBaseHelper.CATEGORY_ID + " = ?";
        String[] whereArgs = new String[1];
        whereArgs[0] = String.valueOf(deletedItemId);
        baseHelper.getWritableDatabase().delete(DataBaseHelper.TABLE_CATEGORIES, whereCat, whereArgs);

        String whereIncome = DataBaseHelper.INCOME_CATEGORIES + " = ?";
        baseHelper.getWritableDatabase().delete(DataBaseHelper.TABLE_INCOME, whereIncome, whereArgs);

        notifyObserversProcedure("category");
    }

    @Override
    public int getFirstDate(){
        Cursor cursorIncome = baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_FIRST_DATE_INCOME, new String[0]);
        cursorIncome.moveToFirst();
        int minDateIncome = cursorIncome.getInt(cursorIncome.getColumnIndex(DataBaseHelper.ALL_HISTORY_DATE));

        Cursor cursorExpenses = baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_FIRST_DATE_EXPENSES, new String[0]);
        cursorExpenses.moveToFirst();
        int minDateExpenses = cursorExpenses.getInt(cursorExpenses.getColumnIndex(DataBaseHelper.ALL_HISTORY_DATE));

        if (minDateIncome == 0){
            return minDateExpenses;
        }
        if (minDateExpenses == 0){
            return minDateIncome;
        }

        if (minDateIncome <= minDateExpenses){
            return minDateIncome;
        } else {
            return minDateExpenses;
        }
    }

    @Override
    public Cursor getIncomeAvailableColors() {
        String[] args = new String[]{String.valueOf(INCOME_CATEGORIES_TYPE)};
        Cursor cursor = baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_ALL_USED_COLORS, args);
        return cursor;
    }

    @Override
    public Cursor getExpensesAvailableColors() {
        String[] args = new String[]{String.valueOf(OUTCOME_CATEGORIES_TYPE)};
        Cursor cursor = baseHelper.getReadableDatabase().rawQuery(RequestsFactory.SELECT_ALL_USED_COLORS, args);
        return cursor;
    }


}


