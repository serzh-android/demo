package com.erminesoft.my_account.myacount.db;

import android.content.ContentValues;


final class Mapper {

    public static ContentValues convertOutcome(String nameText, int date, int category, int sumCost, boolean isSent) {
        ContentValues cv = new ContentValues();
        cv.put(DataBaseHelper.OUTCOME_CATEGORIES, category);
        cv.put(DataBaseHelper.OUTCOME_NAME, nameText);
        cv.put(DataBaseHelper.OUTCOME_DATE, date);
        cv.put(DataBaseHelper.OUTCOME_SUM, sumCost);
        cv.put(DataBaseHelper.OUTCOME_IS_SYNC, isSent);
        return cv;
    }

    public static ContentValues convertIncome(String nameText, int date, int category, int sumIncome, boolean isSent) {
        ContentValues cv = new ContentValues();
        cv.put(DataBaseHelper.INCOME_CATEGORIES, category);
        cv.put(DataBaseHelper.INCOME_NAME, nameText);
        cv.put(DataBaseHelper.INCOME_DATE, date);
        cv.put(DataBaseHelper.INCOME_SUM, sumIncome);
        cv.put(DataBaseHelper.INCOME_IS_SYNC, isSent);
        return cv;
    }

    public static ContentValues convertCategory(String categoryName, int categoryType, int color, boolean isSent) {
        ContentValues cv = new ContentValues();
        cv.put(DataBaseHelper.CATEGORY_NAME, categoryName);
        cv.put(DataBaseHelper.CATEGORY_TYPE, categoryType);
        cv.put(DataBaseHelper.CATEGORY_IMAGE_ID, color);
        cv.put(DataBaseHelper.CATEGORY_IS_SYNC, isSent);
        return cv;
    }

}

