package com.erminesoft.my_account.myacount.db;

final class RequestsFactory {

    static final String SELECT_COSTS_WITH_CATEGORIES =
            "SELECT "
                    + DataBaseHelper.TABLE_OUTCOME + "." + DataBaseHelper.OUTCOME_NAME + " , "
                    + DataBaseHelper.TABLE_OUTCOME + "." + DataBaseHelper.OUTCOME_DATE + " , "
                    + DataBaseHelper.TABLE_OUTCOME + "." + DataBaseHelper.OUTCOME_CATEGORIES + " , "
                    + DataBaseHelper.TABLE_OUTCOME + "." + DataBaseHelper.OUTCOME_SUM + " , "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_NAME + " , "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_ID
                    + " FROM " + DataBaseHelper.TABLE_CATEGORIES + "," + DataBaseHelper.TABLE_OUTCOME
                    + " WHERE "
                    + DataBaseHelper.TABLE_OUTCOME + "." + DataBaseHelper.OUTCOME_CATEGORIES + " = "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_ID;

    static final String SELECT_INCOMES_WITH_CATEGORIES =
            "SELECT "
                    + DataBaseHelper.TABLE_INCOME + "." + DataBaseHelper.INCOME_NAME + " , "
                    + DataBaseHelper.TABLE_INCOME + "." + DataBaseHelper.INCOME_DATE + " , "
                    + DataBaseHelper.TABLE_INCOME + "." + DataBaseHelper.INCOME_CATEGORIES + " , "
                    + DataBaseHelper.TABLE_INCOME + "." + DataBaseHelper.INCOME_SUM + " , "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_NAME + " , "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_ID
                    + " FROM " + DataBaseHelper.TABLE_CATEGORIES + "," + DataBaseHelper.TABLE_INCOME
                    + " WHERE "
                    + DataBaseHelper.TABLE_INCOME + "." + DataBaseHelper.INCOME_CATEGORIES + " = "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_ID;



    static final String SELECT_GENERAL_COSTS_SUM =
            "SELECT "
                    + "SUM(" + DataBaseHelper.OUTCOME_SUM + ") AS " + DataBaseHelper.OUTCOME_SUM
                    + " FROM " + DataBaseHelper.TABLE_OUTCOME;

    static final String SELECT_GENERAL_COSTS_SUM_BY_DATE =
            "SELECT "
                    + "SUM(" + DataBaseHelper.OUTCOME_SUM + ") AS " + DataBaseHelper.OUTCOME_SUM
                    + " FROM " + DataBaseHelper.TABLE_OUTCOME
                    + " WHERE " + DataBaseHelper.OUTCOME_DATE + " >= ?  AND " + DataBaseHelper.OUTCOME_DATE + " <= ? ";

    static final String SELECT_GENERAL_INCOME_SUM =
            "SELECT "
                    + "SUM(" + DataBaseHelper.INCOME_SUM + ") AS " + DataBaseHelper.INCOME_SUM
                    + " FROM " + DataBaseHelper.TABLE_INCOME;

    static final String SELECT_GENERAL_INCOME_SUM_BY_DATE =
            "SELECT "
                    + "SUM(" + DataBaseHelper.INCOME_SUM + ") AS " + DataBaseHelper.INCOME_SUM
                    + " FROM " + DataBaseHelper.TABLE_INCOME
                    + " WHERE " + DataBaseHelper.INCOME_DATE + " >= ?  AND " + DataBaseHelper.INCOME_DATE + " <= ? ";

    static final String SELECT_UNSENT_CATEGORIES =
            "SELECT * "
                    + "FROM " + DataBaseHelper.TABLE_CATEGORIES
                    + " WHERE "
                    + DataBaseHelper.CATEGORY_IS_SYNC + " = 0";

    static final String SELECT_UNSENT_COSTS =
            "SELECT * "
                    + "FROM " + DataBaseHelper.TABLE_OUTCOME
                    + " WHERE "
                    + DataBaseHelper.OUTCOME_IS_SYNC + " = 0";

    static final String SELECT_UNSENT_INCOMES =
            "SELECT * "
                    + "FROM " + DataBaseHelper.TABLE_INCOME
                    + " WHERE "
                    + DataBaseHelper.INCOME_IS_SYNC + " = 0";

    static final String SELECT_ADDED_COSTS_NAME =
            "SELECT * "
                    + "FROM " + DataBaseHelper.TABLE_CATEGORIES
                    + " WHERE "
                    + DataBaseHelper.CATEGORY_TYPE + " = ?"
                    + " AND " + DataBaseHelper.CATEGORY_NAME + " = ?";

    static final String SELECT_ADDED_INCOMES_NAME =
            "SELECT * "
                    + "FROM " + DataBaseHelper.TABLE_CATEGORIES
                    + " WHERE "
                    + DataBaseHelper.CATEGORY_TYPE + " = ?"
                    + " AND " + DataBaseHelper.CATEGORY_NAME + " = ?";


    static final String SELECT_ALL_HISTORY =
            "SELECT  " + DataBaseHelper.TABLE_INCOME + "." + DataBaseHelper.INCOME_ID + " AS " + DataBaseHelper.ALL_HISTORY_ID + ", "
                    + DataBaseHelper.INCOME_NAME + " AS " + DataBaseHelper.ALL_HISTORY_NAME + ", "
                    + DataBaseHelper.INCOME_SUM + " AS " + DataBaseHelper.ALL_HISTORY_SUM + ", "
                    + DataBaseHelper.INCOME_DATE + " AS " + DataBaseHelper.ALL_HISTORY_DATE + ", "
                    + DataBaseHelper.INCOME_CATEGORIES + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_TYPE + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_TYPE + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_IMAGE_ID + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_IMAGE_ID + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_NAME + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_NAME
                    + " FROM " + DataBaseHelper.TABLE_INCOME + ", " + DataBaseHelper.TABLE_CATEGORIES
                    + " WHERE " + DataBaseHelper.ALL_HISTORY_CATEGORY + "=" + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_ID
                    + " UNION ALL "
                    + "SELECT  " + DataBaseHelper.TABLE_OUTCOME + "." + DataBaseHelper.OUTCOME_ID + " AS " + DataBaseHelper.ALL_HISTORY_ID + ", "
                    + DataBaseHelper.OUTCOME_NAME + " AS " + DataBaseHelper.ALL_HISTORY_NAME + ", "
                    + DataBaseHelper.OUTCOME_SUM + " AS " + DataBaseHelper.ALL_HISTORY_SUM + ", "
                    + DataBaseHelper.OUTCOME_DATE + " AS " + DataBaseHelper.ALL_HISTORY_DATE + ", "
                    + DataBaseHelper.OUTCOME_CATEGORIES + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_TYPE + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_TYPE + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_IMAGE_ID + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_IMAGE_ID + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_NAME + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_NAME
                    + " FROM " + DataBaseHelper.TABLE_OUTCOME + ", " + DataBaseHelper.TABLE_CATEGORIES
                    + " WHERE " + DataBaseHelper.ALL_HISTORY_CATEGORY + "=" + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_ID
                    + " ORDER BY " + DataBaseHelper.ALL_HISTORY_DATE + " DESC ";

    static final String SELECT_ALL_HISTORY_BY_DATE =
            "SELECT  " + DataBaseHelper.TABLE_INCOME + "." + DataBaseHelper.INCOME_ID + " AS " + DataBaseHelper.ALL_HISTORY_ID + ", "
                    + DataBaseHelper.INCOME_NAME + " AS " + DataBaseHelper.ALL_HISTORY_NAME + ", "
                    + DataBaseHelper.INCOME_SUM + " AS " + DataBaseHelper.ALL_HISTORY_SUM + ", "
                    + DataBaseHelper.INCOME_DATE + " AS " + DataBaseHelper.ALL_HISTORY_DATE + ", "
                    + DataBaseHelper.INCOME_CATEGORIES + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_TYPE + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_TYPE + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_IMAGE_ID + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_IMAGE_ID + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_NAME + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_NAME
                    + " FROM " + DataBaseHelper.TABLE_INCOME + ", " + DataBaseHelper.TABLE_CATEGORIES
                    + " WHERE " + DataBaseHelper.ALL_HISTORY_CATEGORY + "=" + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_ID
                    + " AND " + DataBaseHelper.ALL_HISTORY_DATE + " >= ?  AND " + DataBaseHelper.ALL_HISTORY_DATE + " <= ? "
                    + " UNION ALL "
                    + "SELECT  " + DataBaseHelper.TABLE_OUTCOME + "." + DataBaseHelper.OUTCOME_ID + " AS " + DataBaseHelper.ALL_HISTORY_ID + ", "
                    + DataBaseHelper.OUTCOME_NAME + " AS " + DataBaseHelper.ALL_HISTORY_NAME + ", "
                    + DataBaseHelper.OUTCOME_SUM + " AS " + DataBaseHelper.ALL_HISTORY_SUM + ", "
                    + DataBaseHelper.OUTCOME_DATE + " AS " + DataBaseHelper.ALL_HISTORY_DATE + ", "
                    + DataBaseHelper.OUTCOME_CATEGORIES + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_TYPE + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_TYPE + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_IMAGE_ID + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_IMAGE_ID + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_NAME + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_NAME
                    + " FROM " + DataBaseHelper.TABLE_OUTCOME + ", " + DataBaseHelper.TABLE_CATEGORIES
                    + " WHERE " + DataBaseHelper.ALL_HISTORY_CATEGORY + "=" + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_ID
                    + " AND " + DataBaseHelper.ALL_HISTORY_DATE + " >= ?  AND " + DataBaseHelper.ALL_HISTORY_DATE + " <= ? "
                    + " ORDER BY " + DataBaseHelper.ALL_HISTORY_DATE + " DESC ";


    static final String SELECT_INCOME_HISTORY =
            "SELECT  " + DataBaseHelper.TABLE_INCOME + "." + DataBaseHelper.INCOME_ID + " AS " + DataBaseHelper.ALL_HISTORY_ID + ", "
                    + DataBaseHelper.INCOME_NAME + " AS " + DataBaseHelper.ALL_HISTORY_NAME + ", "
                    + DataBaseHelper.INCOME_SUM + " AS " + DataBaseHelper.ALL_HISTORY_SUM + ", "
                    + DataBaseHelper.INCOME_DATE + " AS " + DataBaseHelper.ALL_HISTORY_DATE + ", "
                    + DataBaseHelper.INCOME_CATEGORIES + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_TYPE + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_TYPE + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_IMAGE_ID + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_IMAGE_ID + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_NAME + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_NAME
                    + " FROM " + DataBaseHelper.TABLE_INCOME + ", " + DataBaseHelper.TABLE_CATEGORIES
                    + " WHERE " + DataBaseHelper.ALL_HISTORY_CATEGORY + "=" + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_ID
                    + " ORDER BY " + DataBaseHelper.ALL_HISTORY_DATE + " DESC ";

    static final String SELECT_INCOME_HISTORY_BY_DATE =
            "SELECT  " + DataBaseHelper.TABLE_INCOME + "." + DataBaseHelper.INCOME_ID + " AS " + DataBaseHelper.ALL_HISTORY_ID + ", "
                    + DataBaseHelper.INCOME_NAME + " AS " + DataBaseHelper.ALL_HISTORY_NAME + ", "
                    + DataBaseHelper.INCOME_SUM + " AS " + DataBaseHelper.ALL_HISTORY_SUM + ", "
                    + DataBaseHelper.INCOME_DATE + " AS " + DataBaseHelper.ALL_HISTORY_DATE + ", "
                    + DataBaseHelper.INCOME_CATEGORIES + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_TYPE + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_TYPE + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_IMAGE_ID + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_IMAGE_ID + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_NAME + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_NAME
                    + " FROM " + DataBaseHelper.TABLE_INCOME + ", " + DataBaseHelper.TABLE_CATEGORIES
                    + " WHERE " + DataBaseHelper.ALL_HISTORY_CATEGORY + "=" + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_ID
                    + " AND " + DataBaseHelper.ALL_HISTORY_DATE + " >= ?  AND " + DataBaseHelper.ALL_HISTORY_DATE + " <= ? "
                    + " ORDER BY " + DataBaseHelper.ALL_HISTORY_DATE + " DESC ";



    static final String SELECT_OUTCOME_HISTORY =
            "SELECT  " + DataBaseHelper.TABLE_OUTCOME + "." + DataBaseHelper.OUTCOME_ID + " AS " + DataBaseHelper.ALL_HISTORY_ID + ", "
                    + DataBaseHelper.OUTCOME_NAME + " AS " + DataBaseHelper.ALL_HISTORY_NAME + ", "
                    + DataBaseHelper.OUTCOME_SUM + " AS " + DataBaseHelper.ALL_HISTORY_SUM + ", "
                    + DataBaseHelper.OUTCOME_DATE + " AS " + DataBaseHelper.ALL_HISTORY_DATE + ", "
                    + DataBaseHelper.OUTCOME_CATEGORIES + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_TYPE + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_TYPE + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_IMAGE_ID + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_IMAGE_ID + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_NAME + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_NAME
                    + " FROM " + DataBaseHelper.TABLE_OUTCOME + ", " + DataBaseHelper.TABLE_CATEGORIES
                    + " WHERE " + DataBaseHelper.ALL_HISTORY_CATEGORY + "=" + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_ID
                    + " ORDER BY " + DataBaseHelper.ALL_HISTORY_DATE + " DESC ";

    static final String SELECT_OUTCOME_HISTORY_BY_DATE =
            "SELECT  " + DataBaseHelper.TABLE_OUTCOME + "." + DataBaseHelper.OUTCOME_ID + " AS " + DataBaseHelper.ALL_HISTORY_ID + ", "
                    + DataBaseHelper.OUTCOME_NAME + " AS " + DataBaseHelper.ALL_HISTORY_NAME + ", "
                    + DataBaseHelper.OUTCOME_SUM + " AS " + DataBaseHelper.ALL_HISTORY_SUM + ", "
                    + DataBaseHelper.OUTCOME_DATE + " AS " + DataBaseHelper.ALL_HISTORY_DATE + ", "
                    + DataBaseHelper.OUTCOME_CATEGORIES + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_TYPE + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_TYPE + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_IMAGE_ID + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_IMAGE_ID + ", "
                    + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_NAME + " AS " + DataBaseHelper.ALL_HISTORY_CATEGORY_NAME
                    + " FROM " + DataBaseHelper.TABLE_OUTCOME + ", " + DataBaseHelper.TABLE_CATEGORIES
                    + " WHERE " + DataBaseHelper.ALL_HISTORY_CATEGORY + "=" + DataBaseHelper.TABLE_CATEGORIES + "." + DataBaseHelper.CATEGORY_ID
                    + " AND " + DataBaseHelper.ALL_HISTORY_DATE + " >= ?  AND " + DataBaseHelper.ALL_HISTORY_DATE + " <= ? "
                    + " ORDER BY " + DataBaseHelper.ALL_HISTORY_DATE + " DESC ";


    static final String SELECT_CATEGORIES_BY_TYPE =
            "SELECT * "
                    + "FROM " + DataBaseHelper.TABLE_CATEGORIES
                    + " WHERE "
                    + DataBaseHelper.CATEGORY_TYPE + " = ?"
                    + " ORDER BY " + DataBaseHelper.CATEGORY_NAME + " COLLATE NOCASE";

    static final String SELECT_CATEGORY_BY_TYPE_BY_ID =
            "SELECT * "
                    + "FROM " + DataBaseHelper.TABLE_CATEGORIES
                    + " WHERE "
                    + DataBaseHelper.CATEGORY_TYPE + " = ?"
                    + " AND " + DataBaseHelper.CATEGORY_ID + " = ? ";

    static final String SELECT_FIRST_DATE_INCOME =
            "SELECT  "
                    + " min(" + DataBaseHelper.INCOME_DATE  + ") AS " + DataBaseHelper.ALL_HISTORY_DATE
                    + " FROM " + DataBaseHelper.TABLE_INCOME;
//                    + " UNION ALL "
//                    + "SELECT  "
//                    + " min(" +DataBaseHelper.OUTCOME_DATE + ") AS " + DataBaseHelper.ALL_HISTORY_DATE
//                    + " FROM " + DataBaseHelper.TABLE_OUTCOME;

    static final String SELECT_FIRST_DATE_EXPENSES =
//            "SELECT  "
//                    + " min(" + DataBaseHelper.INCOME_DATE  + ") AS " + DataBaseHelper.ALL_HISTORY_DATE
//                    + " FROM " + DataBaseHelper.TABLE_INCOME
//                    + " UNION ALL "
                    "SELECT  "
                    + " min(" +DataBaseHelper.OUTCOME_DATE + ") AS " + DataBaseHelper.ALL_HISTORY_DATE
                    + " FROM " + DataBaseHelper.TABLE_OUTCOME;

    static final String SELECT_ALL_USED_COLORS =
            "SELECT * FROM "
                    + DataBaseHelper.TABLE_CATEGORIES
                    + " WHERE "  + DataBaseHelper.CATEGORY_TYPE + " = ?"
                    + " ORDER BY "
                    + DataBaseHelper.CATEGORY_IMAGE_ID;
}