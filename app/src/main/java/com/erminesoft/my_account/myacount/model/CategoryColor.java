package com.erminesoft.my_account.myacount.model;

public class CategoryColor {

    private int res;
    private int checkedRes;
    private boolean isLocked;
    private int globalPosition;


    public int getRes() {
        return res;
    }

    public void setUnCheckedRes(int res) {
        this.res = res;
    }

    public int getCheckedRes() {
        return checkedRes;
    }

    public void setCheckedRes(int checkedRes) {
        this.checkedRes = checkedRes;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    public int getGlobalPosition() {
        return globalPosition;
    }

    public void setGlobalPosition(int globalPosition) {
        this.globalPosition = globalPosition;
    }
}
