package com.erminesoft.my_account.myacount.model;


public class Entry {
    private float val;
    private int xIndex;

    public float getVal() {
        return val;
    }

    public void setVal(float val) {
        this.val = val;
    }

    public int getxIndex() {
        return xIndex;
    }

    public void setxIndex(int xIndex) {
        this.xIndex = xIndex;
    }
}
