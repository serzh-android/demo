package com.erminesoft.my_account.myacount.model;


public class HistoryDay {

    private String dayOfWeek;
    private String day;
    private String nameMonth;
    private String monthOfYear;
    private String year;
    private int sumDay;
    private boolean noOperations;

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getDay() {
        return day;
    }

    public String getMonthOfYear() {
        return monthOfYear;
    }

    public void setMonthOfYear(String monthOfYear) {
        this.monthOfYear = monthOfYear;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getNameMonth() {
        return nameMonth;
    }

    public void setNameMonth(String nameMonth) {
        this.nameMonth = nameMonth;
    }

    public int getSumDay() {
        return sumDay;
    }

    public void setSumDay(int sumDay) {
        this.sumDay = sumDay;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public boolean isNoOperations() {
        return noOperations;
    }

    public void setNoOperations(boolean noOperations) {
        this.noOperations = noOperations;
    }
}
