package com.erminesoft.my_account.myacount.model;


public class HistoryMonth {

    private String nameMonth;
    private String monthOfYear;
    private String year;
    private int sumMonth;
    private boolean noOperations;

    public String getNameMonth() {
        return nameMonth;
    }

    public void setNameMonth(String nameMonth) {
        this.nameMonth = nameMonth;
    }

    public String getMonthOfYear() {
        return monthOfYear;
    }

    public void setMonthOfYear(String monthOfYear) {
        this.monthOfYear = monthOfYear;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getSumMonth() {
        return sumMonth;
    }

    public void setSumMonth(int sumMonth) {
        this.sumMonth = sumMonth;
    }

    public boolean isNoOperations() {
        return noOperations;
    }

    public void setNoOperations(boolean noOperations) {
        this.noOperations = noOperations;
    }
}
