package com.erminesoft.my_account.myacount.util;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.util.Log;

import com.erminesoft.my_account.myacount.R;
import com.erminesoft.my_account.myacount.model.CategoryColor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ColorUtil {

    public static List<CategoryColor> initColors(Context context, Cursor cursor) {

        TypedArray colorsUnpressed = context.getResources().obtainTypedArray(R.array.background_categories_color);
        TypedArray colorsPressed = context.getResources().obtainTypedArray(R.array.background_categories_color_pressed);

        Set<Integer> existColors = parseCursor(cursor);

        List<CategoryColor> categoryColorList = new ArrayList<>(colorsUnpressed.length());
        CategoryColor color;
        for (int i = 0; i < colorsUnpressed.length(); i++) {
            color = new CategoryColor();
            color.setCheckedRes(colorsPressed.getColor(i,0));
            color.setUnCheckedRes(colorsUnpressed.getColor(i,0));
            color.setLocked(existColors.contains(i));
            color.setGlobalPosition(i);
            categoryColorList.add(color);
        }

        return categoryColorList;
    }

    private static Set<Integer> parseCursor(Cursor cursor) {
        Set<Integer> integers = new HashSet<>(cursor.getCount());
        if (cursor.getCount() < 1){
            return integers;
        }

        cursor.moveToFirst();
        do {
            integers.add(cursor.getInt(cursor.getColumnIndex("categories_image_id")));
        } while (cursor.moveToNext());

        return integers;
    }
}
