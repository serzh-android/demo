package com.erminesoft.my_account.myacount.util;


import android.util.Log;

import com.erminesoft.my_account.myacount.model.HistoryDay;
import com.erminesoft.my_account.myacount.model.HistoryMonth;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public final class DateUtil {

    public static final String DATE_FORMAT_D_M_Y = "dd.MM.yyyy";
    public static final String DATE_FORMAT_M_Y = "MM.yyyy";
    public static final String DATE_FORMAT_H = "HH";
    public static final String DATE_FORMAT_D = "dd";
    public static final String DATE_FORMAT_D_OF_WEEK = "EEE";
    public static final String DATE_FORMAT_M = "MM";
    public static final String DATE_FORMAT_M_NAME = "MMM";
    public static final String DATE_FORMAT_Y = "yyyy";

    public static String dateToFormatString(long creationTime, String dateFormat) {
        return new SimpleDateFormat(dateFormat, Locale.getDefault()).format(new Date(creationTime));
    }

    public static int getStartOfDay(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_D_M_Y, Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(format.parse(date));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return (int) (calendar.getTimeInMillis() / 1000);
    }

    public static int getEndOfDayInMillis(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_D_M_Y, Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(format.parse(date));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);

        return (int) (calendar.getTimeInMillis() / 1000);
    }


    public static int getStartOfMonth(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_M_Y, Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(format.parse(date));
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return (int) (calendar.getTimeInMillis() / 1000);
    }

    public static int getEndOfMonthInMillis(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_M_Y, Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(format.parse(date));
        int countMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, countMonth);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);

        return (int) (calendar.getTimeInMillis() / 1000);
    }


    public static int getStartOfYear(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_Y, Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(format.parse(date));
        calendar.set(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return (int) (calendar.getTimeInMillis() / 1000);
    }

    public static int getEndOfYearInMillis(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_Y, Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(format.parse(date));
        calendar.set(Calendar.MONTH, 12);
        calendar.set(Calendar.DAY_OF_MONTH, 31);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);

        return (int) (calendar.getTimeInMillis() / 1000);
    }


    public static List<String> getYearList(int firstDate, int lastDate) {
        List<String> yearList = new ArrayList<>();

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(new Date(firstDate * (long) 1000));
        cal1.set(Calendar.MONTH, 0);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(new Date(lastDate * (long) 1000));
        cal2.set(Calendar.DAY_OF_MONTH, 31);

        while (!cal1.after(cal2)) {
            yearList.add(new SimpleDateFormat(DATE_FORMAT_Y, Locale.getDefault()).format(cal1.getTime()));
            cal1.add(Calendar.YEAR, 1);
        }

        return yearList;
    }

    public static List<String> getMonthList(int firstDate, int lastDate) {

        List<String> monthList = new ArrayList<>();

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(new Date(firstDate * (long) 1000));

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(new Date(lastDate * (long) 1000));
        cal2.set(Calendar.DAY_OF_MONTH, 31);

        while (!cal1.after(cal2)) {
            monthList.add(new SimpleDateFormat(DATE_FORMAT_M_Y, Locale.getDefault()).format(cal1.getTime()));
            cal1.add(Calendar.MONTH, 1);
        }
        return monthList;
    }


    public static List<String> getDayList(int firstDate, int lastDate) {

        List<String> dayList = new ArrayList<>();

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(new Date(firstDate * (long) 1000));

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(new Date(lastDate * (long) 1000));

        while (!cal1.after(cal2)) {
            dayList.add(new SimpleDateFormat(DATE_FORMAT_D_M_Y, Locale.getDefault()).format(cal1.getTime()));
            cal1.add(Calendar.DAY_OF_MONTH, 1);
        }
        return dayList;
    }


    public static List<HistoryDay> getCountDayOfMonthList(String dateMonth, int firstDbDate) throws ParseException {
        List<HistoryDay> historyDayList = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_M_Y, Locale.getDefault());
        Date date1 = format.parse(dateMonth);
        String monthName = new SimpleDateFormat(DATE_FORMAT_M_NAME, Locale.getDefault()).format(date1);
        String monthOfYear = new SimpleDateFormat(DATE_FORMAT_M, Locale.getDefault()).format(date1);
        String year = new SimpleDateFormat(DATE_FORMAT_Y, Locale.getDefault()).format(date1);

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        cal1.set(Calendar.DAY_OF_MONTH, 2);
        int firstMonth = 0;
        Calendar calendarFirstDate = Calendar.getInstance();
        calendarFirstDate.setTimeInMillis((long) firstDbDate * 1000);

        while (!cal1.after(calendarFirstDate)){
            cal1.add(Calendar.DAY_OF_MONTH, 1);
            firstMonth++;
        }


        int countDay = cal1.getActualMaximum(Calendar.DAY_OF_MONTH);
        if (countDay < 1){
            return historyDayList;
        }
        cal1.set(Calendar.DAY_OF_MONTH, countDay);

        Calendar calendarNow = Calendar.getInstance();

        while (cal1.after(calendarNow)) {
            cal1.add(Calendar.DAY_OF_MONTH, -1);
            countDay--;
        }

        for (int i = countDay; i > firstMonth; i--) {
            String dayOfWeek = new SimpleDateFormat(DATE_FORMAT_D_OF_WEEK, Locale.getDefault()).format(cal1.getTime());
            String day = new SimpleDateFormat(DATE_FORMAT_D, Locale.getDefault()).format(cal1.getTime());
            HistoryDay historyDay = new HistoryDay();

            historyDay.setDay(day);
            historyDay.setNameMonth(monthName);
            historyDay.setDayOfWeek(dayOfWeek);
            historyDay.setMonthOfYear(monthOfYear);
            historyDay.setYear(year);
            historyDayList.add(historyDay);
            cal1.add(Calendar.DAY_OF_MONTH, -1);
        }
        return historyDayList;
    }


    public static List<HistoryMonth> getCountMonthOfYearList(String dateYear, int firstDbDate) throws ParseException {
        List<HistoryMonth> historyMonthList = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_Y, Locale.getDefault());
        Date date1 = format.parse(dateYear);
        String year = new SimpleDateFormat(DATE_FORMAT_Y, Locale.getDefault()).format(date1);

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        cal1.set(Calendar.MONTH, 1);
        int firstMonth = 0;
        int countMonth = 12;

        Calendar calendarFirstDate = Calendar.getInstance();
        calendarFirstDate.setTimeInMillis((long) firstDbDate * 1000);

        while (!cal1.after(calendarFirstDate)){
            cal1.add(Calendar.MONTH, 1);
            firstMonth++;
        }
        Calendar calendarNow = Calendar.getInstance();
        cal1.set(Calendar.MONTH, 11);
        while (cal1.after(calendarNow)) {
            cal1.add(Calendar.MONTH, -1);
            countMonth--;
        }

        for (int i = countMonth; i > firstMonth; i--) {
            String monthName = new SimpleDateFormat(DATE_FORMAT_M_NAME, Locale.getDefault()).format(cal1.getTime());
            String monthOfYear = new SimpleDateFormat(DATE_FORMAT_M, Locale.getDefault()).format(cal1.getTime());

            HistoryMonth historyMonth = new HistoryMonth();
            historyMonth.setNameMonth(monthName);
            historyMonth.setMonthOfYear(monthOfYear);
            historyMonth.setYear(year);
            historyMonthList.add(historyMonth);
            cal1.add(Calendar.MONTH, -1);
        }
        return historyMonthList;
    }

}
