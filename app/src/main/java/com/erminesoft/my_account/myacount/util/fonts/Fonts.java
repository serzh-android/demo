package com.erminesoft.my_account.myacount.util.fonts;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;

public final class Fonts {

    private static final String FONTS_PATH = "fonts/";

    private volatile static Fonts instance;
    private AssetManager assetManager;

    private Fonts() {
    }

    public static Fonts getInstance() {
        Fonts localInstance = instance;

        if (localInstance == null) {
            synchronized (Fonts.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = instance = new Fonts();
                }
            }
        }

        return localInstance;
    }

    public void init(Context context) {
        assetManager = context.getAssets();
    }

    public Typeface getTypeface(String name) {
        return Typeface.createFromAsset(assetManager, FONTS_PATH + name);
    }
}
