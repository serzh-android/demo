package com.erminesoft.my_account.myacount.util.fonts;


public class Roboto {
    public static final String ROBOTO_REGULAR = "Roboto-Regular.ttf";

    public static final String ROBOTO_MEDIUM = "Roboto-Medium.ttf";

    public static final String ROBOTO_BOLD = "Roboto-Bold.ttf";
}
